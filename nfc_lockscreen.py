#!/usr/bin/python	
# -*-coding:Utf-8 -*
"""
Author: Korigan
Comunity: http://hackbbs.org

nfc_lockscreen allow user to launch screensaver with any nfc tag
Anyone can adapt it to execute any action on your computer.
Enjoy :-)

Dependency:
pip install nfcpy
pip install pyusb
"""
import nfc
import nfc.ndef
import time
from subprocess import Popen
from subprocess import call

def connected(tag): detect_tag(tag); return False

def detect_tag(tag):
  global switch_screensaver
  print(tag)
  if "Type4ATag MIU=251 FWT=0.077329" in str(tag):
    print("Good tag")
    if switch_screensaver:
      Popen(["xscreensaver","-no-splash"])
      call(["xscreensaver-command", "-lock"])
      print("1")
      pass
    else:
      call(["pkill","xscreensaver"])
      print("0")
      pass

clf = nfc.ContactlessFrontend('usb')
switch_screensaver = 0
while True:
  switch_screensaver = (switch_screensaver+1) % 2
  clf.connect(rdwr={'on-connect': connected})
  time.sleep(2)
